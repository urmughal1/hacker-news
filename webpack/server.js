const webpackDevServer = require('webpack-dev-server');
const webpack = require('webpack');
const path = require("path");

const config = require('./webpack.dev');
const options = {
  contentBase: path.resolve(__dirname,'../src'),
  hot: true,
  host: 'localhost',
  inline:true,
  historyApiFallback: true
};

webpackDevServer.addDevServerEntrypoints(config, options);
const compiler = webpack(config);
const server = new webpackDevServer(compiler, options);

server.listen(8080, 'localhost', () => {
  console.log('dev server listening on port 8080');
});
