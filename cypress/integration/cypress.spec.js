/**
 * Home page test case
 */
describe('Single page application', () => {

  it('Root Page of the App', () => {
    cy.visit('/')

    cy.wait(5000)
  })
});

/**
 * Checking the API URL Before Listing the Houses test case
 */
describe('Checking the API Before Listing the stories', () => {
  it('Its Working', () => {

    cy.visit(' https://hacker-news.firebaseio.com/v0/item/8863.json?print=pretty')

    cy.wait(5000)
  })
});

/**
 * story list page test case
 */
describe("check the New TAB ",()=>{
  it("Story List page",()=>{

    cy.visit("http://localhost:8080/")

    cy.contains('New').click()

    cy.wait(5000)
  })
})

/**
 * story list page test case
 */
describe("check the Ask TAB ",()=>{
  it("Story List page",()=>{

    cy.visit("http://localhost:8080/")

    cy.contains('Ask').click()

    cy.wait(5000)
  })
})

/**
 * story list page test case
 */
describe("check the Jobs TAB ",()=>{
  it("Story List page",()=>{

    cy.visit("http://localhost:8080/")

    cy.contains('Jobs').click()

    cy.wait(5000)
  })
})


/**
 * story list page test case
 */
describe("check the Show TAB ",()=>{
  it("Story List page",()=>{

    cy.visit("http://localhost:8080/")

    cy.contains('Show').click()

    cy.wait(5000)
  })
})

/**
 * Base url page test case
 */
describe('Test the Base Url ', () => {

  it('Home Page Loaded', () => {
    cy.visit('/')

    cy.wait(5000)
  })
})

/**
 * Testing the detail on the comment Page test case
 */
describe("Testing the detail on the comment detail Page",()=>{
  it("comment Page",()=>{

    cy.visit("http://localhost:8080/comments/27948008")

    cy.wait(5000)
  });
});

/**
 * Testing the detail on the comment Page test case
 */
describe("Testing the detail on the comment detail Page",()=>{
  it("comment Page",()=>{

    cy.visit("http://localhost:8080/comments/27948008")

    cy.get('a').click()

    cy.wait(10000)
  });
});