# hacker-news
First,Thanks for your consideration.

If you have any questions, regarding anything please don't hesitate to contact me. I tried not to over-engineer the things, but my focus was to be able to use them as interactive as possible within a specific time frame.

This Task was more focused on the front end where I had to use the 
[Hacker News](https://github.com/HackerNews/API/blob/master/README.md)
link provided in the task description. I tried my self to restricted functionality and was more focused on putting my effort on enhance the functionality.


## Get started
Let's get into it, to start lets have a look at the repo what included in it

### Structure of the Repo
1. cypress-> consists of testing modules
2. dist-> production related to the folder.
3. src -> main folder for application, contain (code, images, etc)
4. Webpack -> this is a very interesting folder that contains the configuration for the development and production-related deployment.
5. babel.config.js file -> babel configuration 
6. cypress.json -> cypress testing configuration 
7. index.html -> starting point of application
8. Readme.md -> instruction and description of the task 
9. testing.sh -> rto automate the tests
10. demo -> sample running app video and/screen screenshots

### App in Action

clone the repo by running this command
```sh
$ git clone https://gitlab.com/urmughal1/hacker-news.git
```
This command will install the required packages.
```
$ npm install 
```
This is the interesting command that will run the application
```
$ npm run dev
```
application can be seen on the following Ip address
```
http://localhost:8080
```

## Application Architecture 
The Architecture of the application is very simple. it consists of 4 components [Baselayout] responsible for the main page, [StoriesList] will contain the list of the first 20 of stories, [StoryComments] will contain the functionality to manipulate the story comments and in the last [comments] which will have all comments.


Some features of the APP
- This app is always taking newest data by chunk of 20 
- App can be Extended very easily
- Hungarian notation as coding style
- cleaned code
- responsive/simple design

## Testing
I am using the cypress tool for testing which is very interactive and providing the
full package to test as unit, integration, and interfaces.

I have created different test cases,and the code can be found in the following directory
```sh
<RootDir>/cypress/integration/cypress.spec.js
```
to run the test-cases please run the following commands
```sh
$ npm install cypress --save-dev
$ ./node_modules/.bin/cypress run
```
OR
```sh
$ ./testing.sh
```
Results are visible on the terminal and cypress also creates an interactive video that can be found in the following directory after running the above command.

```sh
<RootDir>/cypress/videos/cypress.spec.mp4
```
## Production
For production, you need to run the following command
```sh
$ npm run build
```
after running this command you will see a dist folder that will contain
the distributable

to test the production is working or not please run this command
```sh
$ npm run build
$ serve -s dist
```
application can be seen on the following Ip address
```
http://localhost:5000
```
### Core Libraries/Dependencies
- webpack 5.46.0 
- webpack-cli 4.7.2
- webpack-dev-server 3.11.2
- bootstrap-vue
- axios
- cypress 8.0.0
- vue 3.70
- node v14.16.0
- npm 6.14.11

## Further Improvements/Extra things can be done
This is not the end. There is always need of improvement.if I will have more time than,
I would like to do the followings
* More functionality test with jest
* Would like to implement in docker to deploy using gitlab-ci.yml 
* improvement on comment section as design

## Expected results
Screenshots are available in ```ROOT_DIR/demo``` directory

## Task Time Analysis
1. API Review -> 30 min
2. Preparing the skeleton directory -> 30 min
3. Coding -> 2 hour
4. Readme -> 50 min
5. Screenshot and demo -> 30 min
7. Testing with bug fixing -> 1 hour

### Further More
Previously, I have done some work on DevOps, python, vue.js, PHP, Doctrine, Liquibase, Elastic Search, Mysql and etc....
1. live deployed work on AWS using (ECS, EC2, S3, EFS etc)for the Organization
    - https://staging.inpera.xyz
    - https://testserver.inpera.net
    - https://saas.inpera.net

