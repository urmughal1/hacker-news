import Vue from "vue";
import VueRouter from "vue-router";
import BaseLayout from "../components/BaseLayout.vue";
import StoryComments from "../components/StoryComments.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "BaseLayout",
    component: BaseLayout,
  },
  {
    path: "/comments/:id",
    name: "StoryComments",
    component: StoryComments,
    meta:{fail: '/'}
  },
];

export default new VueRouter({
  mode: "history",
  routes:routes,
});

